//
// Created by aleksandr on 18.07.2021.
//

#include <string>
#include <iostream>

class Person {
public:
    void SetName(const std::string& name) {
        name_ = name;
    }
    void SetAge(uint age) {
        age_ = age;
    }
    void SetWeight(uint weight) {
        weight_ = weight;
    }

    void Print() {
        std::cout << "Name:\t" << name_ << std::endl;
        std::cout << "Age:\t" << age_ << std::endl;
        std::cout << "Gender:\t" << gender_ << std::endl;
        std::cout << "Weight:\t" << weight_ << std::endl;
    }
private:
    std::string name_;
    uint age_;
    uint gender_;
    uint weight_;
};


class Student: public Person {
public:
    Student(): Person() {
        counter++;
    }

    void SetStudyTime(uint study_time) {
        study_time_ = study_time;
    }
    void IncreaseStudyTime() {
        study_time_++;
    }

    static uint counter;
private:
    uint study_time_;
};

uint Student::counter = 0;

int main() {
    Student st;
    st.SetName("sasha");
    st.SetAge(20);
    st.SetWeight(80);
    st.SetStudyTime(2);
    st.IncreaseStudyTime();

    Student st1;
    st1.SetName("andrey");
    st1.SetAge(20);
    st1.SetWeight(68);
    st1.SetStudyTime(1);
    st1.IncreaseStudyTime();

    std::cout << Student::counter << std::endl;

    st.Print();

    return 0;
}

