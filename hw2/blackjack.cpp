//
// Created by aleksandr on 18.07.2021.
//

enum Value;

enum Suit;

class Card {
public:
    Setters;
    Getters;

private:
    Value value_;
    Suit suit_;
};

class Counter() {
public:
    Setters;
    Getters;

    CheckValueCard(Card& card);
    Increase();
private:
    uint counter = 0;
}

class Hand() {
public:
    Setters;
    Getters;
private:
    std::vector<Card> cards_;
}

class Player() {
public:
    Setters;
    Getters;

    DrawCard();
private:
    Hand hand_;
    Counter counter_;
}

class Dealer: public Player {
public:
    Setters;
    Getters;

    ShowCard();
};

class Human: public Player {
public:
    Setters;
    Getters;

    Split();
    Double();
    Stop();
};

class PlayerHandler {
public:
    Setters;
    Getters;

    CheckForBlackJack();
    CheckForBust();
private:
    Player& player_;
};

class Game {
public:
    Start();
};