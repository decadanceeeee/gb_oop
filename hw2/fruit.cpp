//
// Created by aleksandr on 18.07.2021.
//

#include <iostream>

class Fruit {
public:
    Fruit(std::string name, std::string color): name_(name), color_(color) {}
    std::string getName() {
        return name_;
    }
    std::string getColor() {
        return color_;
    }

    void SetName(const std::string& name) {
        name_ = name;
    }

    void SetColor(const std::string& color) {
        color_ = color;
    }
private:
    std::string name_;
    std::string color_;
};

class Apple: public Fruit {
public:
    Apple(std::string color) : Fruit("apple", color) {}
};

class Banana: public Fruit {
public:
    Banana(): Fruit("banana", "yellow") {}
};

class GrannySmith: public Apple {
public:
    GrannySmith(): Apple("green") {
        SetName("Granny Smith apple");
    }
};

int main() {
    Apple a("red");
    Banana b;
    GrannySmith c;

    std::cout << "My " << a.getName() << " is " << a.getColor() << ".\n";
    std::cout << "My " << b.getName() << " is " << b.getColor() << ".\n";
    std::cout << "My " << c.getName() << " is " << c.getColor() << ".\n";

    return 0;
}
