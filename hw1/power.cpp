//
// Created by aleksandr on 18.07.2021.
//

#include <cmath>

class Power {
public:
    void Set(double x, double y) {
        x_ = x;
        y_ = y;
    }

    double Calculate() {
        return pow(x_, y_);
    }

private:
    double x_ = 1;
    double y_ = 0;
};