//
// Created by aleksandr on 18.07.2021.
//

#include <cstdint>
#include <iostream>

class RGBA {
public:
    RGBA(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) : m_red_(red), m_green_(green), m_blue_(blue), m_alpha_(alpha) {}

    void Print() {
        std::cout << static_cast<int>(m_red_) << ":"
                  << static_cast<int>(m_green_) << ":"
                  << static_cast<int>(m_blue_) << ":"
                  << static_cast<int>(m_alpha_);
    }


private:
    uint8_t m_red_ = 0;
    uint8_t m_green_ = 0;
    uint8_t m_blue_ = 0;
    uint8_t m_alpha_ = 255;
};