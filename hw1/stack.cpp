//
// Created by aleksandr on 18.07.2021.
//

#include <iostream>

class Stack
{
public:
    Stack() : top(-1) {}

    Stack(const Stack&) = delete;
    Stack& operator=(const Stack&) = delete;

    Stack(Stack&&) = delete;
    Stack& operator=(Stack&& st) = delete;


    bool push(int a) {
        if (top_ + 1 >= 10) {
            return false;
        }

        buffer_[++top_] = a;
        return true;
    }

    int pop() {
        if (top_ != -1) {
            return buffer_[top_--];
        }

        std::cout << "No elements" << std::endl;
        return -1;
    }

    void reset() {
        top_ = -1;
    }

    void print() {
        std::cout << "( ";
        for (int i = 0; i < top_ + 1; ++i) {
            std::cout << buffer_[i] << " ";
        }
        std::cout << ")" << std::endl;
    }

    bool is_empty() const { return top_ == -1; }

private:
    int buffer_[10];
    int top_; // top element index
};

int main() {

    Stack stack;
    stack.reset();
    stack.print();

    stack.push(3);
    stack.push(7);
    stack.push(5);
    stack.print();

    stack.pop();
    stack.print();

    stack.pop();
    stack.pop();
    stack.print();

    return 0;

}
